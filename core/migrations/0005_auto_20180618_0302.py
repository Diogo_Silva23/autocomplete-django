# Generated by Django 2.0.6 on 2018-06-18 03:02

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('core', '0004_auto_20180618_0022'),
    ]

    operations = [
        migrations.AlterField(
            model_name='country',
            name='continent',
            field=models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.CASCADE, to='core.Continent'),
        ),
    ]
