from django.urls import path
from core.views import *

app_name='core'

urlpatterns = [
    path('create/', CreateCountry.as_view(), name="create" ),
    path('list/', ListCoutryView.as_view(), name="country_list" ),
    path('get_places/', get_places, name='get_places')
]
