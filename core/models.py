from django.db import models
from django.urls import reverse 
# Create your models here.

class Continent(models.Model):
    name = models.CharField(max_length=255, unique=True)

    class Meta:
        verbose_name_plural = "Continent"

    def __str__(self):
        return self.name

class Country(models.Model):
    name = models.CharField(max_length=255)
    continent = models.name = models.ForeignKey('Continent', on_delete=models.CASCADE, blank=True, null=True)
    class Meta:
        verbose_name_plural = "Country"
        
    def __str__(self):
        return self.name

    def get_absolute_url(self):
        return reverse('coutry_list', kwargs={'pk':self.pk})


