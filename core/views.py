from django.shortcuts import render
from django.contrib import messages
from django.urls import reverse_lazy
from django.views.generic   import CreateView, TemplateView, ListView
from django.http import HttpResponse

from rest_framework import viewsets

from core.models import *
from core.forms import CoutryForm
from core.serializers import *

import json

class ContinentViewSet(viewsets.ModelViewSet):
    queryset = Continent.objects.all().order_by('id')
    serializer_class = ContinentSerializer

def get_places(request):
    if request.is_ajax():

        q = request.GET.get('term', '')
        print(q)
        continents = Continent.objects.filter(name__icontains=q)
        result = []
        for continent in continents:
            continent_json = {}
            continent_json = continent.name 
            result.append(continent_json)
        data = json.dumps(result)
    else:
        data = 'fail'
    mimetype = 'application/json'
    return HttpResponse(data, mimetype)

# Create your views here.
class Home(TemplateView):
    template_name = 'index.html'

class CreateCountry(CreateView):
    model = Country
    form_class = CoutryForm
    template_name = 'form.html'
    success_url = reverse_lazy('core:create')

    def form_valid(self,form ):    
        
        """If the form is valid, save the associated model."""
        continent = Continent.objects.get(name=self.request.POST.get('continent'))
        self.object = form.save(commit=False)
        self.object.continent = continent
        self.object.save()
        return super().form_valid(form)


    def get_success_url(self):
        if self.success_url:
            messages.success(self.request, 'Criado com successo')
            return self.success_url.format(**self.object.__dict__)
        else:
            raise ImproperlyConfigured("No URL to redirect to. Provide a success_url.")

class ListCoutryView(ListView):
    pass