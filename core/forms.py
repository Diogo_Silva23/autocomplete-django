from django import forms
from . import models

class CoutryForm(forms.ModelForm):
    class Meta:
        model = models.Country
        fields = ('name',)

